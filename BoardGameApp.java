import java.util.Scanner;
public class BoardGameApp{
	public static void main(String[] args){
		playGame(); // calls playGame
		
	}
	public static void playGame(){
		Board grid = new Board(); //creates a new board object
		System.out.println("Welcome User!");
		int numCastles = 7; //Sets variables of turns, castles
		int turns = 0;
		int noRemainingCastles = 0;
		int lastTurn = 8;
		
		
		while(numCastles >noRemainingCastles && turns < lastTurn){ //runs while there are more than 0 castles and less than 8 turns
			System.out.println(grid); //prints grid
			System.out.println("The number of castles remaining is: " + numCastles);//prints how many castles must be placed
			System.out.println("Turn: " + turns); //prints the turn
			
			Scanner reader = new Scanner(System.in); 
			System.out.println("Please input a row"); //asks user to print a row and column
			int row = Integer.parseInt(reader.nextLine());
			System.out.println("Please input a column");
			int col = Integer.parseInt(reader.nextLine());
			
			int checkInputs = grid.placeToken(row, col); //sets the return value of placeToken into an int that will be checked
			
			while(checkInputs < 0){ //if checkInputs is negative it will ask the user to re-input the values
				System.out.println("Please re-enter the row and column");
				System.out.println("Please input a row");
				row = Integer.parseInt(reader.nextLine());
				System.out.println("Please input a column");
				col = Integer.parseInt(reader.nextLine());
				checkInputs = grid.placeToken(row, col);
			}
			if(checkInputs == 1){ //A 1 means there is a wall meaning that they couldnt place a castle on this turn
				System.out.println("There is a wall here!");
			}
			if(checkInputs == 0){ //a Castle was succesfully place here
				System.out.println("The castle tile was succesfully place!");
				numCastles--;
			}
			turns++;
		}
		System.out.println(grid); //prints the grid
		if(numCastles == 0){ //if there are no more castles that means the user wins
			System.out.println("Congratulations!! You have won!!");
		}
		else{ //if they still have castles after turn 8 then they lose
			System.out.println("Uh Oh, youve lost");
		}
	}
	
	
}