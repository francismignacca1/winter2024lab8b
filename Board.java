import java.util.Random;
public class Board{
	private Tile[][] grid; //field that represents the board of tic tac toe
	private int lengthOfBoard = 5;
	
	public Board(){ //constructor of the board that sets it to 3x3
		Random rng = new Random(); 
		this.grid = new Tile[lengthOfBoard][lengthOfBoard]; // sets the size of the board's length and width to lengthOfBoard
		
		for(int i = 0; i < this.grid.length; i++){ //Start of the loop to initialize what is on each tile in the board
		int randIndex = rng.nextInt(grid[i].length); //Grabs a random number and sets it to a variable
			for(int j = 0; j < this.grid.length; j++){
				if(j == randIndex){ //if j equlas the random number then that tile will not be blank but a hidden wall
					this.grid[i][j] = Tile.HIDDEN_WALL;
				}
				else{
					this.grid[i][j] = Tile.BLANK; // If its not the random number it will be set to blank
				}
			}
		}
	}
	public String toString(){ // this toString method sets the tiles on the board to what the constructor in the Tile class set them to and skips a line for it to make it look like a board
		String result = "";
		for(int i = 0; i < this.grid.length; i++){
			for(int j = 0; j < this.grid.length; j++){
				result += this.grid[i][j].getName() + " ";
			}
			result += "\n";
		}
		return result;
	}
	public int placeToken(int row, int col){// this method checks to make sure the number that is given is within the board then replaces then checks whats on the tile given and returns an int depeding on the outcome
		if(row < 0 || row > lengthOfBoard - 1  && col < 0 || col > lengthOfBoard - 1){//The row and column given was not a valid number inside the board
			return -2;
		}
		else if(this.grid[row][col] == Tile.CASTLE || this.grid[row][col] == Tile.WALL){//There is already something on this tile
			return -1;
		}
		else if(this.grid[row][col] == Tile.HIDDEN_WALL){ //This tile was a hidden wall that will be switched to a wall
			this.grid[row][col] = Tile.WALL;
			return 1;
		}
		else{ //A 0 means that a castle can be placed at this position meaning it was previously and empty tile
			this.grid[row][col] = Tile.CASTLE;
			return 0;
		}
	}
	
	
	
	
	
	
}