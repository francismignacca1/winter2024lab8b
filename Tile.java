public enum Tile{
	BLANK("_"), //sets enums for all possible tiles
	WALL("W"),
	HIDDEN_WALL("_"),
	CASTLE("C");
	
	private final String name;
	
	private Tile(String name){
		this.name = name;
	}
	public String getName(){
		return this.name;
	}
}